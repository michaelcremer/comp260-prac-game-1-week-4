﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	public float speed = 4.0f;
	public float turnSpeed = 180.0f;
	public Transform target;
	public Transform target2;
	public Vector2 heading = Vector3.right;
	private Vector2 direction;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance (transform.position, target.position) < Vector3.Distance (transform.position, target2.position)) {
			direction = target.position - transform.position;
		} else {
			direction = target2.position - transform.position;
		}
			
		float angle = turnSpeed * Time.deltaTime;

		if (direction.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);
		} else {
			heading = heading.Rotate (-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);

		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay (transform.position, direction);
	}
}