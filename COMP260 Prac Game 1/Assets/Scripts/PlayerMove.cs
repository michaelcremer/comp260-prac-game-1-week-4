﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f;

	// Use this for initialization
	void Start () {
	
	}

	public string whichPlayerMovementHorizontal = "Horizontal (Player _)";
	public string whichPlayerMovementVertical = "Vertical (Player _)";

	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis (whichPlayerMovementHorizontal);
		direction.y = Input.GetAxis (whichPlayerMovementVertical);

		Vector2 velocity = direction * maxSpeed;

		transform.Translate (velocity * Time.deltaTime);
	}

}
